export const my_is_posi_neg = (nbr) => {
    if (nbr === 0)
        return "NEUTRAL"
    if (typeof nbr !== "number" || nbr > 0)
        return "POSITIF"
    return "NEGATIF"
}