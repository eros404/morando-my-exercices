export const my_size_alpha = (str) => {
    if (typeof str !== "string")
        return 0;
    for (var i = 0; str[i]; i++)
        continue;
    return i;
}