import { my_size_alpha } from "./exercise-4.js"

export const my_array_alpha = (str) => {
    const strSize = my_size_alpha(str)
    const result = []
    for (let i = 0; i < strSize; i++)
        result[i] = str[i]
    return result;
}