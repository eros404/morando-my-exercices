import { my_display_alpha } from "./exercise-1.js"
import { my_size_alpha } from "./exercise-4.js"

export const my_display_alpha_reverse = () => {
    let result = ""
    let alpha = my_display_alpha()
    for (let i = my_size_alpha(alpha) - 1; i >= 0; i--)
        result += alpha[i]
    return result;
}