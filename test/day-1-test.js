import * as assert from "assert"
import { my_sum } from "../day-1/exercise-0.js"
import { my_display_alpha } from "../day-1/exercise-1.js"
import { my_display_alpha_reverse } from "../day-1/exercise-2.js"
import { my_alpha_number } from "../day-1/exercise-3.js"
import { my_size_alpha } from "../day-1/exercise-4.js"
import { my_array_alpha } from "../day-1/exercise-5.js"
import { my_length_array } from "../day-1/exercise-6.js"
import { my_is_posi_neg } from "../day-1/exercise-7.js"

describe('Exo 0', function () {
  describe('#my_sum()', function () {
    it('should return 0 if a or b has no value', () => {
      assert.strictEqual(my_sum(null, null), 0);
    });
    it('should return 0 if a or b is not a number', () => {
      assert.strictEqual(my_sum("a", 9), 0);
    });
    it('should return 52 if a is 12 and b is 40', () => {
      assert.strictEqual(my_sum("a", 9), 0);
    });
  });
});

describe('Exo 1', function () {
  describe('#my_display_alpha()', function () {
    it('should return all letters in the alphabetical order', () => {
      assert.strictEqual(my_display_alpha(), "abcdefghijklmnopqrstuvwxyz");
    });
  });
});

describe('Exo 2', function () {
  describe('#my_display_alpha_reverse()', function () {
    it('should return all letters in the reverse alphabetical order', () => {
      assert.strictEqual(my_display_alpha_reverse(), "zyxwvutsrqponmlkjihgfedcba");
    });
  });
});

describe('Exo 3', function () {
  describe('#my_alpha_number()', function () {
    it('should return the given number as a string', () => {
      assert.strictEqual(my_alpha_number(9), "9");
    });
  });
});

describe('Exo 4', function () {
  describe('#my_size_alpha()', function () {
    it('should return 0 when a number is given', () => {
      assert.strictEqual(my_size_alpha(9), 0);
    });
    it('should return 0 when a null is given', () => {
      assert.strictEqual(my_size_alpha(null), 0);
    });
    it('should return 4 when "toto" is given', () => {
      assert.strictEqual(my_size_alpha("toto"), 4);
    });
  });
});

describe('Exo 5', function () {
  describe('#my_array_alpha()', function () {
    it('should return the given string as a array of characters', () => {
      assert.deepStrictEqual(my_array_alpha("toto"), ["t", "o", "t", "o"]);
    });
  });
});

describe('Exo 6', function () {
  describe('#my_length_array()', function () {
    it('should return the 4 when the given array is ["t", "o", "t", "o"]', () => {
      assert.strictEqual(my_length_array(["t", "o", "t", "o"]), 4);
    });
    it('should return the 3 when the given array is ["l", "o", "l"]', () => {
      assert.strictEqual(my_length_array(["l", "o", "l"]), 3);
    });
  });
});

describe('Exo 7', function () {
  describe('#my_is_posi_neg()', function () {
    it('should return "NEUTRAL" when the given number is 0', () => {
      assert.strictEqual(my_is_posi_neg(0), "NEUTRAL");
    });
    it('should return "POSITIF" when the given number is 4', () => {
      assert.strictEqual(my_is_posi_neg(4), "POSITIF");
    });
    it('should return "POSITIF" when the given number is null', () => {
      assert.strictEqual(my_is_posi_neg(null), "POSITIF");
    });
    it('should return "POSITIF" when the given number is undefined', () => {
      assert.strictEqual(my_is_posi_neg(undefined), "POSITIF");
    });
    it('should return "NEGATIF" when the given number is -4', () => {
      assert.strictEqual(my_is_posi_neg(-4), "NEGATIF");
    });
  });
});